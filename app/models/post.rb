class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	validates_presence_of :title
	validates_presence_of :body
end

#has_one
#has_many
#has_and_belongs_to_many